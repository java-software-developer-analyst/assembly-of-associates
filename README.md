# Desafio Técnico - NTConsult

_Projeto alvo de constante evolução_

## Observações:

Para a utilização do banco de dados **não** persistido em memória (o **h2**), é necessário (**neste caso**) ter o PostgreSQL devidamente instalado e configurado.

Conforme as configurações do arquivo [application-dev.properties](https://gitlab.com/java-software-developer-analyst/assembly-of-associates/-/blob/main/backend/src/main/resources/application-dev.properties). Ainda sobre este arquivo, importante observar:

1. Descomentar tirando o " # " apenas na primeira vez que levantar o servidor local do spring e verificar se o arquivo "create.sql" foi criado, para então abri-lo e copiar o conteúdo para a criação das tabelas no seu SGBD
2. Caso o **arquivo "create.sql" já exista**, desconsiderar o 1º processo. Bastando apenas abrir o arquivo, copiar e colar no seu SGBD.


### Rotas das APIs (local):

Listar todos os associados: http://localhost:8080/associates

Listar um associado pelo seu ID: http://localhost:8080/associates/{id}

Listar todas as pautas: http://localhost:8080/agendas

Listar uma pauta por id: http://localhost:8080/agendas/{id}

Registrar uma pauta: http://localhost:8080/agendas/register

Caso queira alimentar seu banco de dados com informações de forma mais prática, atentar-se para o arquivo [import.sql](https://gitlab.com/java-software-developer-analyst/assembly-of-associates/-/blob/main/backend/src/main/resources/import.sql)


# Objetivo
<details><summary>{+ Objetivo +}</summary>

No cooperativismo, cada associado possui um voto e as decisões são tomadas em assembleias, por votação. A partir disso, você precisa criar uma solução back-end para gerenciar essas sessões de votação.
Essa solução deve ser executada na nuvem e promover as seguintes funcionalidades através de uma APT REST:

- [x] Cadastrar uma nova pauta;

- [x] Abrir uma sessão de votação em uma pauta (a sessão de votação deve ficar aberta por um tempo determinado na chamada de abertura ou 1 minuto por default);

- [x] Receber os votos dos associados em pautas (os votos são apenas ‘Sim’/’Não’. Cada associado é identificado por um id único e pode votar apenas uma vez por pauta);

- [ ] Contabilizar os votos e dar o resultado da votação na pauta.

Para fins de exercício, a segurança das interfaces pode ser abstraída e qualquer chamada para as interfaces podem ser consideradas como autorizadas. A escolha da linguagem, framework e bibliotecas é livre (desde que não infrinja os direitos de uso).
É importante que as pautas e os votos sejam persistidos e que não sejam perdidos com o restart da aplicação.
</details>

# Tarefa Bônus

<details><summary>{- Tarefa Bônus 1 – Integração com sistemas externos -}</summary>

- [ ] Integrar com um sistema que verifique, a partir do CPF do associado, se ele pode votar
    
    - GET https://user-info.herokuapp.com/users/{cpf}
    
    - Caso o CPF não seja válido, a API retornará HTTP Status 404 (Not found). Você pode usar os geradores de CPF para gerar CPFs válidos;
    
    - Caso o CPF seja válido, a API retornará se o usuário pode (ABLE_TO_VOTE) ou não pode (UNABLE_TO_VOTE) executar a operação

```
    // GET /users/19839091069
    // 200 OK
    { 
        "status": "ABLE_TO_VOTE"
    }

    // GET/users/6228960806`
    // 200 OK`
    { 
        "status": "UNABLE_TO_VOTE"
    }
```

</details>

<details><summary>{- Tarefa Bônus 2 – Mensageria e filas -}</summary>

- [ ] O resultado da votação precisa ser informado para o restante da plataforma, isso deve ser feito preferencialmente através de mensageria. Quando a sessão de votação fechar, poste uma mensagem com o resultado da votação.
</details>

<details><summary>{- Tarefa Bônus 3 – Performance -}</summary>

- [ ] Imagine que sua aplicação possa ser usada em cenários que existam centenas de milhares de votos. Ela deve se comportar de maneira performática nesses cenários;

- [ ] Testes de performance são uma boa maneira de garantir e observar como sua aplicação se comporta
</details>

<details><summary>{+ Tarefa Bônus 4 – Versionamento de API +}</summary>


- [x] Como você versionaria a API da sua aplicação? Que estratégia usar?

**Resposta:** Versionaria em forma de monorepo. Projeto no formato de microserviço com integração contínua.

</details>
