package com.ntconult.desafio.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.ntconult.desafio.dto.associate.AssociateResponseDTO;
import com.ntconult.desafio.models.Associate;
import com.ntconult.desafio.repositories.AssociateRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class AssociateServiceTest {
	
	static class AgendaServiceTestConfiguration {
		
		@Bean
		@Qualifier
		public AssociateService associateServiceTest() {
			return new AssociateService();
		}
	}
	
	@Autowired
	AssociateService service;
	
	@MockBean
	AssociateRepository repository;
	
	
//	## Tests ##
	
	@Test
	@DisplayName("Find all associates")
	public void findAll() {
		
		// expected
		List<AssociateResponseDTO> mockResultDTO = new ArrayList<>();
		mockResultDTO.add(new AssociateResponseDTO("03887344006", "Ana Gray"));
		mockResultDTO.add(new AssociateResponseDTO("97611132059", "Bob Green"));
		
		// actual
		List<Associate> associates = new ArrayList<>();
		associates.add(new Associate(1L, "03887344006", "Ana Gray", null) );
		associates.add(new Associate(2L, "97611132059", "Bob Green", null) );
		
		Mockito.when(repository.findAll()).thenReturn(associates);
		
		List<AssociateResponseDTO> resultDTO = new ArrayList<>();
		resultDTO = service.findAll();
		
		
		Assertions.assertEquals(mockResultDTO, resultDTO);
	}
	
	@Test
	@DisplayName("Find one associate by ID")
	public void findById() {
		
		// ## expected
		Long mockId;
		mockId= 1L;
		
		Associate mockAssociate;
		mockAssociate = new Associate(mockId, "95451506027", "Carl Blue", null);
		
		AssociateResponseDTO mockResultDTO;
		mockResultDTO = new AssociateResponseDTO("95451506027", "Carl Blue");
		
		Mockito.when(repository.findById(mockAssociate.getId())).thenReturn(java.util.Optional.of(mockAssociate));
		Optional<Associate> assocOpt = repository.findById(mockId);
		
		// testing parameters
		Assertions.assertEquals(mockAssociate.getId(), assocOpt.get().getId());
		
		
		// ## actual
		Long id = 1L;
		
		AssociateResponseDTO resultDTO;
		resultDTO = service.findById(id);
		
		
		// testing return of service
		Assertions.assertEquals(mockResultDTO, resultDTO);
		
	}
	
	


}
