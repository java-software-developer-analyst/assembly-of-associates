package com.ntconult.desafio.services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.ntconult.desafio.dto.agenda.AgendaResponseDTO;
import com.ntconult.desafio.models.Agenda;
import com.ntconult.desafio.repositories.AgendaRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class AgendaServiceTest {
	
	static class AgendaServiceTestConfiguration {
		
		@Bean
		@Qualifier
		public AgendaService agendaService() {
			return new AgendaService();
		}
	}
	
	@Autowired
	AgendaService service;
	
	@MockBean
	AgendaRepository repository;
	
	
//	## Tests ##
	@Test
	@DisplayName("Find all agendas")
	public void findAll() {
		
		// ## expected
		LocalDateTime now;
		now = LocalDateTime.now();
		
		long timeMinutes;
		timeMinutes= 5L;
		
		List<AgendaResponseDTO> mockResultDTO = new ArrayList<>();
		mockResultDTO.add(new AgendaResponseDTO("La Pergunta?", now, timeMinutes, now, now.plusMinutes(timeMinutes)));
		mockResultDTO.add(new AgendaResponseDTO("Cuma?", now, timeMinutes, now, now.plusMinutes(timeMinutes)));
		
		// ## actual
		List<Agenda> agendas = new ArrayList<>();
		agendas.add(new Agenda(1L, "La Pergunta?", now, timeMinutes, now, now.plusMinutes(timeMinutes), null));
		agendas.add(new Agenda(2L, "Cuma?", now, timeMinutes, now, now.plusMinutes(timeMinutes), null));
		
		Mockito.when(repository.findAll()).thenReturn(agendas);
		
		List<AgendaResponseDTO> resultDTO = new ArrayList<>();
		resultDTO = service.findAll();
		
		Assertions.assertEquals(mockResultDTO, resultDTO);
	}
	
	
	@Test
	@DisplayName("Find one agenda by ID")
	public void findByID() {
		
		// ## expected
		Long mockId = 2L;
		
		LocalDateTime now;
		now = LocalDateTime.now();
		
		long timeMinutes;
		timeMinutes = 5;
		
		Agenda mockAgenda;
		mockAgenda = new Agenda(mockId, "La Pergunta?", now, timeMinutes, now, now.plusMinutes(timeMinutes), null);
		
		AgendaResponseDTO mockResultDTO;
		mockResultDTO = new AgendaResponseDTO("La Pergunta?", now, timeMinutes, now, now.plusMinutes(timeMinutes));
		
		Mockito.when(repository.findById(mockAgenda.getId())).thenReturn(java.util.Optional.of(mockAgenda));
		Optional<Agenda> agendOpt = repository.findById(mockId);
		
		// testing parameters
		Assertions.assertEquals(mockAgenda.getId(), agendOpt.get().getId());
		
		
		// ## actual
		Long id = 2L;
		
		AgendaResponseDTO resultDTO;
		resultDTO = service.findById(id);
		
		// testing return of service
		Assertions.assertEquals(mockResultDTO, resultDTO);
	}
}
