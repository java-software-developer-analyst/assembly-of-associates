package com.ntconult.desafio.services;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ntconult.desafio.dto.agenda.AgendaRequestDTO;
import com.ntconult.desafio.dto.agenda.AgendaResponseDTO;
import com.ntconult.desafio.models.Agenda;
import com.ntconult.desafio.repositories.AgendaRepository;

@Service
public class AgendaService {
	
	@Autowired
	private AgendaRepository repository;
	
	
	public List<AgendaResponseDTO> findAll() {
		List<Agenda> result;
		result = repository.findAll();

		List<AgendaResponseDTO> resultDTO;
		resultDTO = result.stream().map(x -> new AgendaResponseDTO(x)).collect(Collectors.toList());
		
		
//		return new ArrayList<>();
		return resultDTO;
	}
	
	
	public AgendaResponseDTO findById(Long id) {
		try {
			Optional<Agenda> result;
			result = repository.findById(id);
			
			AgendaResponseDTO resultDTO;
			resultDTO = new AgendaResponseDTO(result.get());
			
			
//			return new AgendaResponseDTO();
			return resultDTO;
			
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException(e.getMessage(), e.getCause());
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException(e.getMessage());
		}
	}
	
	
	public AgendaResponseDTO register(AgendaRequestDTO agendaReqDTO) {
		
		long time = (agendaReqDTO.getSessionTimeMinutes() > 0L) ? agendaReqDTO.getSessionTimeMinutes() : 1L;
		agendaReqDTO.setSessionTimeMinutes(time);

		Agenda agendaParam;
		agendaParam = agendaReqDTO.toAgenda();

		
		Agenda result;
		result = repository.save(agendaParam);
		
		agendaParam.setEndOfSesion(agendaParam.getStartOfSession().plusMinutes(time));
		result = repository.saveAndFlush(agendaParam);
		
		AgendaResponseDTO resultDTO;
		resultDTO = new AgendaResponseDTO(result);
		
		return resultDTO;

	}

}
